import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    initial(): Promise<string>;
    addItems(): any;
    getTotalAdsCount(): any;
    scrapeTruckItem(): any;
}
