export declare class AppService {
    private initialUrl;
    private cheerioRes;
    addItems(): Promise<any>;
    getTotalAdsCount(): Promise<any>;
    scrapeTruckItem(): Promise<any>;
}
