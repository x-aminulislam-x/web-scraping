"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("axios");
const cheerio_1 = require("cheerio");
const pretty_1 = require("pretty");
let AppService = class AppService {
    constructor() {
        this.initialUrl = 'https://www.otomoto.pl/ciezarowe/uzytkowe/mercedes-benz/od-2014/q-actros?search%5Bfilter_enum_damaged%5D=0&search%5Border%5D=created_at+%3Adesc';
    }
    async cheerioRes() {
        const { data } = await axios_1.default.get(this.initialUrl);
        return cheerio_1.default.load(data);
    }
    async addItems() {
        try {
            const $ = await this.cheerioRes();
            const addsItem = $('.ooa-1hab6wx article');
            const adds = [];
            addsItem.each((idx, elm) => {
                const add = { url: '', id: '' };
                add.url = $(elm)
                    .children('div')
                    .children('h2')
                    .children('a')
                    .attr('href');
                add.id = $(elm).attr('id');
                adds.push(add);
            });
            return adds;
        }
        catch (err) {
            return err;
        }
    }
    async getTotalAdsCount() {
        try {
            const $ = await this.cheerioRes();
            const listItems = $('.ooa-1hab6wx article');
            return listItems.length;
        }
        catch (err) {
            return err;
        }
    }
    async scrapeTruckItem() {
        try {
            const $ = await this.cheerioRes();
            const item = $('#6100010951 div');
            const itemURL = item.children('h2').children('a').attr('href');
            const { data } = await axios_1.default.get(itemURL);
            const $itemData = cheerio_1.default.load(data);
            console.log((0, pretty_1.default)($.html()));
            const listItems = $itemData('.offer-params ul li');
            const itemId = $itemData('#ad_id');
            const itemPrice = $itemData('.offer-price__number');
            console.log(itemId.text());
            let adds = {};
            listItems.each((indx, elm) => {
                const add = {};
                const key = $itemData(elm).children('span').text();
                add[key] = $itemData(elm).children('div').text().trim();
                adds = Object.assign(Object.assign({}, adds), add);
            });
            console.log(adds);
            const requireOBJ = {
                id: itemId.text() || null,
                title: adds['Marka pojazdu'] || null,
                price: itemPrice.text() || null,
                'registration-date': adds['Pierwsza rejestracja'] || null,
                'production-date': adds['Rok produkcji'] || null,
                mileage: adds['Przebieg'] || null,
                power: adds['Moc'] || null,
            };
            return requireOBJ;
        }
        catch (err) {
            return err;
        }
    }
};
AppService = __decorate([
    (0, common_1.Injectable)()
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map