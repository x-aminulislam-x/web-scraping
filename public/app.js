"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const cheerio_1 = require("cheerio");
const url = 'https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3';
async function scrapeData() {
    try {
        const { data } = await axios_1.default.get(url);
        const $ = cheerio_1.default.load(data);
        console.log($);
    }
    catch (err) {
        console.error(err);
    }
}
scrapeData();
//# sourceMappingURL=app.js.map