import { Injectable } from '@nestjs/common';
import axios from 'axios';
import cheerio from 'cheerio';

@Injectable()
export class AppService {
  private initialUrl =
    'https://www.otomoto.pl/ciezarowe/uzytkowe/mercedes-benz/od-2014/q-actros?search%5Bfilter_enum_damaged%5D=0&search%5Border%5D=created_at+%3Adesc';

  // function for fetch the page to use ;
  private async cheerioRes(): Promise<any> {
    // Fetch HTML of the page we want to scrape
    const { data } = await axios.get(this.initialUrl);
    // Load HTML we fetched in the previous line
    return cheerio.load(data);
  }

  // Add addItems function that fetches item urls + item ids
  async addItems(): Promise<any> {
    try {
      const $ = await this.cheerioRes();

      // Select all the adds items in class
      const addsItem = $('.ooa-1hab6wx article');
      // Stores data for all countries

      const adds = [];
      // Use .each method to loop through the li we selected
      addsItem.each((idx, elm) => {
        // Object holding data for each url/id
        const add = { url: '', id: '' };
        // Select the text content of a and span elements
        // Store the text content in the above object
        add.url = $(elm)
          .children('div')
          .children('h2')
          .children('a')
          .attr('href');

        add.id = $(elm).attr('id');

        // Populate adds array with truck data
        adds.push(add);
      });

      return adds;
    } catch (err) {
      // return error if failed to fetch
      return err;
    }
  }

  // getTotalAdsCount function - shows how many total ads exist for the provided initial url
  async getTotalAdsCount(): Promise<any> {
    try {
      const $ = await this.cheerioRes();
      const listItems = $('.ooa-1hab6wx article');
      const totalAdds = { totalAdds: listItems.length };
      return totalAdds;
    } catch (err) {
      // return error if failed to fetch
      return err;
    }
  }

  // scrapeTruckItem function - that scrapes the actual ads and parses into the format: item id, title, price, registration date, production date, mileage, power
  async scrapeTruckItem(): Promise<any> {
    try {
      const $ = await this.cheerioRes();
      // targeting specific adds with id
      const add = $('#6100010951 div');
      // getting specific adds url to fetch it
      const addURL = add.children('h2').children('a').attr('href');
      const { data } = await axios.get(addURL);

      // load it to $adds
      const $adds = cheerio.load(data);

      // scraping adds details
      const listItems = $adds('.offer-params ul li');
      // scraping add item id
      const addItemId = $adds('#ad_id');
      // scraping add item price
      const addItemPrice = $adds('.offer-price__number');

      let adds = {};

      listItems.each((indx, elm) => {
        const add = {};
        // assigning key to value
        const key = $adds(elm).children('span').text();
        // assigning value to key of object
        add[key] = $adds(elm).children('div').text().trim();
        // storing collected data into object
        adds = { ...adds, ...add };
      });

      // created an object as required formate
      const requireOBJ = {
        id: addItemId.text() || null,
        title: adds['Marka pojazdu'] || null,
        price: addItemPrice.text() || null,
        'registration-date': adds['Pierwsza rejestracja'] || null,
        'production-date': adds['Rok produkcji'] || null,
        mileage: adds['Przebieg'] || null,
        power: adds['Moc'] || null,
      };

      return requireOBJ;
    } catch (err) {
      // return error if failed to fetch
      return err;
    }
  }
}
