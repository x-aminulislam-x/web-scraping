import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async initial() {
    return 'this is web scraping initial route';
  }

  @Get('addItems')
  addItems(): any {
    return this.appService.addItems();
  }

  @Get('getTotalAdsCount')
  getTotalAdsCount(): any {
    return this.appService.getTotalAdsCount();
  }

  @Get('scrapeTruckItem')
  scrapeTruckItem(): any {
    return this.appService.scrapeTruckItem();
  }
}
